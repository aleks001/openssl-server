## Introduction

This repo runs openssl s_server on port 2016.  
This port can be set via the variable **LISTEN_PORT**

## Openshift steps

First login into your openshift and create a new project


### Centos Variant
```
oc new-app https://gitlab.com/aleks001/openssl-server.git --context-dir=centos/default
```

### RHEL7 Variant
```
oc new-app https://gitlab.com/aleks001/openssl-server.git --context-dir=rhel7/default
```

Wait until the builds are done.
The POD will crash because the cert & key is not set at the first runtime.

You can in the meantime create the certificate and key.

## Create cert and key

### Elliptic Curve

```
openssl ecparam -name secp384r1 -genkey -noout -out key.pem
openssl req -x509 -new -key key.pem -out cert.pem -days 365 -nodes \
  -subj '/CN=YOUR_COMMOM_NAME/O=YOUR_ORG/L=YOUR_LOCATION/ST=YOUR_STATE/C=YOUR_COUNTRY'
```

### RSA

```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes \
  -subj '/CN=YOUR_COMMOM_NAME/O=YOUR_ORG/L=YOUR_LOCATION/ST=YOUR_STATE/C=YOUR_COUNTRY'
```

## Add cert and key

```
oc env dc/openssl-server CERT=$(base64 -w 0 < cert.pem) KEY=$(base64 -w 0 < key.pem) 
```

If you need some more parameters for s_server then you can add this into **EXTRA_PARAMS** 

```
oc env dc/openssl-server EXTRA_PARAMS="-bugs -debug -msg -state -tlsextdebug"
```

## Create route

```
oc create route passthrough openssl-server --hostname='YOUR_COMMOM_NAME' --service='openssl-server'
```

Now you can connect to **YOUR_COMMOM_NAME** and see the ouptput of s_server.

## Available variables

| Variable     | Default value |
| :--------     | :--------: |
| LISTEN_PORT  | 2016     |
| CERT         | **EMPTY** |
| KEY          | **EMPTY** |
| EXTRA_PARAMS | **EMPTY** |
| DEBUG        | **EMPTY** |
