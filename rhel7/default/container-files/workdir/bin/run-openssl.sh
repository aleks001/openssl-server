#!/bin/bash


if [ ! -z ${DEBUG} ]; then
set -x
fi

DEFAULT_ARGS=" s_server -www -accept ${LISTEN_PORT} "
CERT_FILE=/tmp/run-openssl.crt
KEY_FILE=/tmp/run-openssl.key


if [ -z ${CERT} ];
then
  echo "need CERT DATA"
  echo "Var CERT is empty"
  exit 1
else
  if [ ! -s ${CERT} ]; then
    echo ${CERT} | base64 -d > ${CERT_FILE}
  else
    CERT_FILE=${CERT}
  fi
fi

if [ -z ${KEY} ];
then
  echo "need KEY DATA"
  echo "Var KEY is empty"
  exit 2
else
  if [ ! -s ${KEY} ]; then
    echo ${KEY} | base64 -d > ${KEY_FILE}
  else
    KEY_FILE=${KEY}
  fi
fi

exec /usr/bin/openssl ${DEFAULT_ARGS} -cert ${CERT_FILE} -key ${KEY_FILE} ${EXTRA_PARAMS}
